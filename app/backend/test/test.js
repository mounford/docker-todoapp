let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server/server');
let databaseConnector = require('../server/DatabaseConnector.js');
let should = chai.should();

var getTodos = '/api/todos',
getTodo = '/api/todo/search?',
updateTodo = '/api/todo/update?',
deleteTodo = '/api/todo/delete?',
solveTodo = '/api/todo/solve?',
createTodo = '/api/todo/create?';

chai.use(chaiHttp);

describe('/GET requests', () => {
before(() => {
  databaseConnector.createTodo('title');
});

  it('getAllTodos_assert200', (done) => {
    chai.request(server)
    .get(getTodos)
    .end((err, res) => {
      res.should.have.status(200);
      done();
    });
  });

  it('getTodo_noId_assert400', (done) => {
    chai.request(server)
    .get(getTodo)
    .end((err, res) => {
      res.should.have.status(400);
      done();
    });
  });

  it('getTodo_withExistingId_assert200', (done) => {
    databaseConnector.getBiggestId().then(id => {
      chai.request(server)
      .get(getTodo+"id="+id)
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
    }).catch(done);
  });

  it('getTodo_withoutExistuingId_assert400', (done) => {
    databaseConnector.getBiggestId().then(id => {
      chai.request(server)
      .get(getTodo+"id="+(id+1))
      .end((err, res) => {
        res.should.have.status(400);
        done();
      });
    }).catch(done);
  });
});

describe('/POST requests', () => {
  before(() => {
    databaseConnector.getTodos();
  });

  describe('createTodo tests', () => {
    it('createTodo_noTitle_assert400', (done) => {
      chai.request(server)
      .post(createTodo)
      .end((err, res) => {
        res.should.have.status(400);
        done();
      });
    });
  
    it('createTodo_existingTitle_assertTodoCreated', (done) => {
      databaseConnector.getBiggestId().then(oldId => {
        chai.request(server)
        .post(createTodo+'title=title')
        .end((err, res) => {
          res.should.have.status(200);
          databaseConnector.getBiggestId().then(newId => {
            oldId.should.not.equal(newId);
            done();
          }).catch(done);
        });
      }).catch(done);
    });
  });

  describe('updateTodo tests', () => {
    it('updateTodo_noTitle_noId_assert400', (done) => {
      chai.request(server)
      .post(updateTodo)
      .end((err, res) => {
        res.should.have.status(400);
        done();
      });
    });
    
    it('updateTodo_existingTitle_noId_assert400', (done) => {
      chai.request(server)
      .post(updateTodo+"title=newTitle")
      .end((err, res) => {
        res.should.have.status(400);
        done();
      });
    });
    
    it('updateTodo_noTitle_validId_assert400', (done) => {
      databaseConnector.createTodo('title').then(todo => {
        chai.request(server)
        .post(updateTodo+"id="+todo.id)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
      }).catch(done);
    });
  
    it('updateTodo_existingTitle_validId_assertUpdate', (done) => {
      databaseConnector.createTodo('title').then(todo => {
        chai.request(server)
        .post(updateTodo+"title=newTitle&id="+todo.id)
        .end((err, res) => {
          res.should.have.status(200);
          databaseConnector.getTodo(todo.id).then(updatedTodo => {
            var titleOld = todo.title;
            var titleNew = updatedTodo.title;
            titleNew.should.not.equal(titleOld);
            titleNew.should.equal('newTitle');
            done();
          }).catch(done);
        });
      }).catch(done);
    });
  });

  describe('solveTodo tests', () => {
    it('solveTodo_noId_assert400', (done) => {
      chai.request(server)
      .post(solveTodo)
      .end((err, res) => {
        res.should.have.status(400);
        done();
      });
    });

    it('solveTodo_invalidId_assert400', (done) => {
      databaseConnector.getBiggestId().then(id => {
        chai.request(server)
        .post(solveTodo+"id="+(id+1))
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
      }).catch(done);
    });

    it('solveTodo_validId_assert200_assertChange', (done) => {
      databaseConnector.getBiggestId().then(id => {
        databaseConnector.getTodo(id).then(todo => {
          chai.request(server)
          .post(solveTodo+"id="+id)
          .end((err, res) => {
            res.should.have.status(200);
            databaseConnector.getTodo(id).then(updatedTodo => {
              oldRes = todo.solved;
              newRes = updatedTodo.solved;
              oldRes.should.not.equal(newRes);
              done();
            }).catch(done);
          });
        }).catch(done);
      }).catch(done);
    });
  });
  
  describe('deleteTodo tests', () => {
    it('deleteTodo_noId_assert400', (done) => {
      chai.request(server)
      .post(deleteTodo)
      .end((err, res) => {
        res.should.have.status(400);
        done();
      });
    });

    it('deleteTodo_invalidId_assert400', (done) => {
      databaseConnector.getBiggestId().then(id => {
        chai.request(server)
        .post(deleteTodo+"id="+(id+1))
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
      }).catch(done);
    });
    
    it('deleteTodo_validId_assert200', (done) => {
      databaseConnector.createTodo('title').then(todo => {
        var oldBiggest = todo.id;
        chai.request(server)
        .post(deleteTodo+"id="+(oldBiggest))
        .end((err, res) => {
          res.should.have.status(200);
          databaseConnector.getBiggestId().then(id => {
            oldBiggest.should.not.equal(id);
            done();
          }).catch(done);
        });
      }).catch(done);
    });
  });
});