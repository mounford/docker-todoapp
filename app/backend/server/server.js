var express = require('express');
var app = express();
var cors = require('cors');
var databaseConnector = require('./DatabaseConnector.js');
var port = 3000;

var getTodos = '/api/todos',
deleteAllTodos = '/api/todos/delete',
solveAllTodos = '/api/todos/solve',
getTodo = '/api/todo/search?',
updateTodo = '/api/todo/update?',
deleteTodo = '/api/todo/delete?',
solveTodo = '/api/todo/solve?',
createTodo = '/api/todo/create?';

var corsOptions = {
	origin: 'http://localhost:4000',
	optionsSuccessStatus: 200
}

app.use(cors(corsOptions));

app.get(getTodos, function (req, res) {
	databaseConnector.getTodos().then(todos => {
		res.set({'Content-Type':'application/json'});
		sendResponse(res, 200, todos);
	})
	.catch(message => {
		res.set({'Content-Type':'text/html'});
		sendResponse(res, 500, message);
	});
});

app.get(getTodo, function(req, res){
	if(req.query.id == null){
		res.set({'Content-Type':'text/html'});
		sendResponse(res, 400, "id has to be set to get a specific todo.")
		return;
	}
	databaseConnector.getTodo(req.query.id)
	.then(todo => {
		res.set({'Content-Type':'application/json'});
		sendResponse(res, 200, todo);
	})
	.catch(message => {
		res.set({'Content-Type':'text/html'});		
		sendResponse(res, 400, message);
	});
});

app.post(deleteAllTodos, function(req, res){
	databaseConnector.deleteAllTodos()
	.then(message => {
		res.set({'Content-Type':'application/json'});
		sendResponse(res, 200, message)
	})
	.catch(message => {
		res.set({'Content-Type':'text/html'});		
		sendResponse(res, 500, message)
	});
});

app.post(solveAllTodos, function(req, res){
	databaseConnector.solveAllTodos()
	.then(message => {
		res.set({'Content-Type':'application/json'});
		sendResponse(res, 200, message);
	})
	.catch(message => {
		res.set({'Content-Type':'text/html'});		
		sendResponse(res, 500, message)
	});
});

app.post(createTodo, function(req, res){
	if(req.query.title == null){
		res.set({'Content-Type':'text/html'});		
		sendResponse(res, 400, "title has to be set to create a todo.");
		return;
	}
	databaseConnector.createTodo(req.query.title)
	.then(todo => {
		res.set({'Content-Type':'application/json'});
		sendResponse(res, 200, todo)
	}).catch(message => {
		res.set({'Content-Type':'text/html'});		
		sendResponse(res, 400, message);
	});
});

app.post(updateTodo, function(req, res){
	res.set({'Content-Type':'text/html'});		
	if(req.query.title == null || req.query.id == null){
		sendResponse(res, 400,"title and id have to be set to update a todo.");
		return;
	}
	databaseConnector.updateTodo(req.query.id, req.query.title)
		.then(message => {		
			sendResponse(res, 200, message);
		})
		.catch(message => {		
			sendResponse(res, 400, message);
	});
});

app.post(solveTodo, function(req, res){
	res.set({'Content-Type':'text/html'});		
	if(req.query.id == null){
		sendResponse(res, 400, "id has to be set to toggle the solved state of a todo.");
		return;
	}
	databaseConnector.toggleSolveTodo(req.query.id)
	.then(message => {
		sendResponse(res, 200, message)
	})
	.catch(message => {
		sendResponse(res, 400, message)
	});
});

app.post(deleteTodo, function(req, res){
	res.set({'Content-Type':'text/html'});
	if(req.query.id == null){
		sendResponse(res, 400, "id has to be set to delete the related todo.");
		return;
	}
	databaseConnector.deleteTodo(req.query.id)
	.then((message) => sendResponse(res, 200, message))
	.catch((message) => sendResponse(res, 400, message));
});

var server = app.listen(port, function () {
	console.log('Example app listening on port 3000!');
});

sendResponse = (res, statusCode, data) => {
	console.log("code: "+statusCode+" data: "+ data);
	res.status(statusCode).send(data);
}

module.exports = app; //for testing
