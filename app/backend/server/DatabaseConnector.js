const sqlite3 = require('sqlite3').verbose();

const CREATE_TODOS_TABLE = 'CREATE TABLE IF NOT EXISTS todos(id INTEGER PRIMARY KEY, title TEXT NOT NULL, solved INTEGER NOT NULL, CHECK (solved = 0 OR solved = 1));',
    GET_TODOS = "SELECT * FROM todos;",
	GET_TODO = "SELECT * FROM todos WHERE id=?;",
	CREATE_TODO = "INSERT INTO todos (id, title, solved) VALUES (?,?,0);"
    UPDATE_TODO = "UPDATE todos SET title = ? where id = ?;",
	TOGGLE_SOLVED_STATE = "UPDATE todos SET solved = CASE WHEN solved = 0 THEN 1 ELSE 0 END WHERE id=?;";
	DELETE_TODO = "DELETE from todos WHERE id=?;",
	DELETE_ALL_TODOS = "DELETE from todos;",
	SOLVE_ALL_TODOS = "UPDATE todos SET solved = 1;";
	GET_ID = "SELECT id from todos WHERE EXISTS id = ?";
const db = new sqlite3.Database(__dirname+'/../db/todos.db', (err) => {
  if (err) {
    return console.error(err.name+" "+err.message);
  }
  console.log('Connected to the in-memory SQlite database.');
});

var idCache = [];
 
db.run(CREATE_TODOS_TABLE);

exports.getTodos = () => new Promise((resolve, reject) => {
	const todos = [];
	idCache = [];
	db.all(GET_TODOS, (err, rows) => {
		if (err) {
			reject(err.message);
		} else {
		for (let row of rows) {
			const todo = {
				id: row.id,
				title: row.title,
				solved: row.solved
			};
			todos.push(todo);
			idCache.push(row.id);
		}
	resolve(todos);
    }
  });
});

exports.deleteTodo = id => new Promise((resolve, reject) => {
	idSafetyCheck(id).then(message => {
		db.run(DELETE_TODO, [id], function(err) {
			if(err){
				reject(err.message);
			} else {
				idCache.splice(idCache.indexOf(parseInt(id)), 1);
				resolve('todo '+ id 	+' was deleted');
			}
		});
	}).catch(message => reject(message));
});

exports.deleteAllTodos = () => new Promise((resolve, reject) => {
	db.run(DELETE_ALL_TODOS, function(err){
		if(err){
			reject(err.message);
		} else {
			idCache = [];
			resolve('all todos have been deleted');
		}
	})
});

exports.solveAllTodos = () => new Propmise((resolve, reject) => {
	db.run(SOLVE_ALL_TODOS, function(err){
		if(err){
			reject(err.message);
		} else {
			resolve('all todos have been solved');
		}
	});
});

exports.createTodo = title => new Promise((resolve, reject) => {
	getNewId().then(id => {
		db.run(CREATE_TODO, [id, title], function(err){
			if(err){
				reject(err.message);
			} else {
				idCache.push(id);
				resolve({id, title, 'solved' : 0});
			}
		});
	})
});

exports.updateTodo = (id, title) => new Promise((resolve, reject) => {
	idSafetyCheck(id).then((message) => {
		db.run(UPDATE_TODO, [title, id], function(err){
			if(err){
				reject(err.message);
			} else {
				resolve({id, title});
			}
		});
	}).catch(message => {
		reject(message);
	});
});

exports.getTodo = id => new Promise((resolve, reject) => {
	idSafetyCheck(id).then(message => {
		db.get(GET_TODO, [id], function(err, row){
			if(err){
				reject("todo doesn't exists");
			} else {
				resolve({id: row.id, title: row.title, solved: row.solved});
			}
		});
	}).catch(message => {
		reject(message);
	});
});

exports.toggleSolveTodo = id => new Promise((resolve, reject) => {
	idSafetyCheck(id).then(message => {
		db.run(TOGGLE_SOLVED_STATE, [id], function(err){
			if(err){
				reject(err.message);
			} else {
				exports.getTodo(id).then(todo => {
					resolve(todo);
				});
			}
		});
	}).catch(message => reject(message));
});

getNewId = () => new Promise((resolve, reject) => {
	exports.getBiggestId().then(id => {
		resolve(id + 1);
	}).catch(id => {
		resolve(id);
	});
});

exports.getBiggestId = () => new Promise((resolve, reject) => {
	db.get('SELECT MAX(id) AS id FROM todos', (err, row) => {
		if(err || row.id == null){
			reject(0);
		} else {
			resolve(row.id);
		}
	});
});

getResolutionOfTodo = (id) => new Promise((resolve, reject) => {
	db.get('SELECT solved FROM todos where ?', [id], (err, row) => {
		if(err){
			reject(0);
		} else {
			resolve(row.solved);
		}
	});
});

idSafetyCheck = (id) => new Promise((resolve, reject) => {
	if(idCache.length == 0 || !idCache.includes(parseInt(id))){
		exports.getTodos().then(() => {
			if(idCache.length == 0 || !idCache.includes(parseInt(id))){
				reject("todo doesn't exist");
			} else {
				resolve('todo exists');
			}
		}).catch(message => {
			reject("todo doesn't exist")
		});
	} else {
		resolve("todo exists");
	}
});