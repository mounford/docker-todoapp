var express = require('express');
var fs = require('fs');
var app = express();
var port = 4000;

var base = '/base.css',
js = '/index.js',
html = '/index.html',
flex = '/flex.css';

app.listen(port, function () {
	console.log('Frontend is listening on port'+ port +'!');
});

app.get('/', (req, res) => {
    console.log(html);
    processRequest('.'+html, 'text/html', res);
});

app.get(js, (req, res) => {
    console.log(js);
    processRequest('.'+js, 'text/javascript', res);
});

app.get(flex, (req, res) => {
    console.log(flex);
    processRequest('.'+flex, 'text/css', res);
});

app.get(base, (req, res) => {
    console.log(base);
    processRequest('.'+base, 'text/css', res);
});

processRequest = (filename, mimeType, res) => {
    getText(filename).then(file => {
        sendResposeFile(file, mimeType, res);
    })
    .catch(message => {
        sendErrorMessage(message, res);
    })
}

getText = (dir) => new Promise((resolve, reject) => {
    fs.readFile(dir, 'binary', (err, file) => {
        if(err){
            reject(err.message);
        } else {
            resolve(file);
        }
    })
});

sendResposeFile = (file, mimeType, res) => {
    res.set({'Content-Type': mimeType});
    res.status(200);
    res.send(file);
}

sendErrorMessage = (message, res) => {
    res.set({'Content-Type':'text/plain'});
    res.status(200);
    res.send(message);
}