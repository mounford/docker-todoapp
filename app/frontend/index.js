const app = new Vue({ 
    el: '#app',
    data: {
        showSolved: false,
        buttonName: "show solved",
        newTodo: "",
        timer: '',
        todos: []
    },
    mounted(){
        this.getTodos();
        this.timer = setInterval(this.getTodos, 3000);
    },
    methods: {
        showHideSolved(){
            this.showSolved = !this.showSolved;
            if(this.showSolved){
                this.buttonName = "hide solved";
            } else {
                this.buttonName = "show solved";
            }
        },

        cancelUpdate(){
            console.log("stopped");
            clearInterval(this.timer);
        },

        resumeUpdate(){
            console.log("resumed");
            this.getTodos();
            this.timer = setInterval(this.getTodos, 3000);
        },
        
        addTodo(){
            if(this.newTodo === "") {
               return;
            }
            const param = encodeURI(this.newTodo);
            console.log("http://localhost:3000/api/todo/create?title="+param);
            this.postData("http://localhost:3000/api/todo/create?title="+param).then(this.getTodos).catch(message => console.log(message));
            this.newTodo = "";
        },

        solveTodo(todo){
            const param = todo.id;
            console.log("http://localhost:3000/api/todo/solve?id="+param);
            this.postData("http://localhost:3000/api/todo/solve?id="+param).then(this.getTodos).catch(message => console.log(message));
        },
        
        deleteTodo(todo){
            const url = "http://localhost:3000/api/todo/delete?id="+todo.id;
            console.log(url);
            this.postData(url).then(this.getTodos).catch(message => console.log(message));
        },
        
        solveAll(){
            this.postData("http://localhost:3000/api/todos/solve").then(this.getTodos).catch(message => console.log(message));
        },
        
        deleteAll(){
            this.postData("http://localhost:3000/api/todos/delete").then(this.getTodos).catch(message => console.log(message));
        },
        
        updateTodo(todo){
            const url = "http://localhost:3000/api/todo/update?id="+todo.id+"&title="+encodeURI(todo.title);
            console.log(url);
            this.postData(url).then(this.getTodos).catch(message => console.log(message));
            document.activeElement.blur();
        },

        async getTodos(){
            const url = 'http://localhost:3000/api/todos';
            console.log(url);
            const response = await fetch(url);
            const data = await response.json();
            this.todos = data;
        },

        async postData(url){
            const response = await fetch(url, {
                method : 'POST',
                mode : 'cors',
                headers: {'Content-Type':'text/html'},
                redirect: "follow",
            });
            return await response;
        }
    }
});